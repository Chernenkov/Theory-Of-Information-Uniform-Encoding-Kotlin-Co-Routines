import kotlinx.coroutines.experimental.*

/**
 * Class for counting probabilities and errors for const-size encoding.
 * @param    seq            input string, from which alphabet is counted, using [HashMap]
 * @param    theN           size of block for encoding
 * @property length         length of input string
 * @property symbols        alphabet, built by message
 * @property probabilities  table: symbol->probability
 * @property entropy        entropy by formula SUM(p * -log2(p))
 *
 * @author Alex Chernenkov
 */
class Probability(private val seq: String, private val theN: Int){
    private val length  = seq.length
    private val symbols = countSymbols(seq)
    private val probabilities = mutableListOf<HashMap<String, Int>>()
    private val entropy = calcEntropy(symbols, length)

    init {
        // add counted sequencies
        probabilities.add(symbols)
        // calculate probabilities for all block sizes up to [theN]
        calcAllProbs()
        // find lost, count P[error]'s
        countErrors()
    }

    /**
     * Function prints sequences turned into probabilities.
     * (Sequency)/(alphabet size) = probability of 1 symbol.
     */
    fun printProbs(i: Int) {
        val itr = probabilities[i].toList().sortedByDescending { it.second }.iterator()
        val alphabet = (Math.pow(seq.length.toDouble(), i + 1.toDouble()))
        while (itr.hasNext()) {
            for (j in 0..11) {
                if (itr.hasNext()) {
                    val next = itr.next()
                    print("${next.first}=")
                    val p = next.second.toDouble() / alphabet
                    print(String.format("%.5f", p))
                    print(" | ")
                } else {
                    break
                }
            }
            print("\n")
        }
    }

    /**
     * Builds alphabet for symbols in string
     */
    private fun countSymbols(seq: String): HashMap<String, Int> {
        val symbols = HashMap<String, Int>()
        seq.forEach {
            val tmp = it.toString()
            if (symbols.contains(tmp)) {
                symbols[tmp] = symbols.getValue(tmp) + 1
            } else {
                symbols.put(tmp, 1)
            }
        }
        return symbols
    }

    /**
     * Function counts entropy
     */
    private fun calcEntropy(symbols: HashMap<String, Int>, sequenceLength: Int): Double {
        var entropy = 0.0
        symbols.forEach { _, value ->
            // H = SUM(p*-log(p))
            entropy += (value.toDouble() / sequenceLength) * (-log2((value.toDouble() / sequenceLength)))
        }
        return entropy
    }

    /**
     * Function calculates probabilities for block sizes from 2 to [theN].
     */
    private fun calcAllProbs() {
        for (i in 2..theN) {
            calcBlockWithSize(i)
        }
    }

    /**
     * Function counts probabilities for block of [blockSize] length
     */
    private fun calcBlockWithSize(blockSize: Int) {
        val newProbMap = HashMap<String, Int>()

        // Controlling co-routine
        runBlocking {
            val extendable = probabilities[blockSize - 2]
            extendable.forEach { elem ->
                // Нагрузочная корутина
                launch {
                    symbols.forEach {
                        newProbMap.put(elem.key + it.key, it.value * elem.value)
                    }
                }.join()
            }
        }
        probabilities.add(newProbMap)
        println("Co-routine for N=$blockSize finished.")
    }

    /**
     * Function counts P[error] for n, while R > H
     */
    private fun countErrors() {
        for (theN in 1..theN) {
            // alphabet takes this number of bits(to code in this alphabet):
            val alphabet = (Math.pow(symbols.size.toDouble(), theN.toDouble()))
            // message length takes this number of bits(size, needed to code the message):
            val seqLength = (Math.pow(seq.length.toDouble(), theN.toDouble()))
            println("N = $theN")
            var n = Math.ceil(log2(alphabet)) // size of table is size of bits,
            // needed to code alphabet
            while (true) {
                val r = n / theN // speed
                print("\tn = $n | R = ${String.format("%.5f", r)} ") // print speed
                if (r > entropy) { // condition check to print '<' or '>'
                    print(">")
                } else {
                    print("<")
                }
                print(" H = $entropy") // print entropy
                var lost = probabilities[theN - 1].size - Math.pow(2.0, n).toInt() // count lost
                lost = if (lost < 0) 0 else lost // check for 2^n(alphabet power) > table size
                //sort probabilities table
                val sorted = probabilities[theN - 1].toList().sortedBy { it.second }
                var pErr = 0.0
                print(" | lost = $lost") // print lost
                for (j in 0 until lost) { //sum of lost probabilities = P[error]
                    pErr += sorted[j].second
                }
                pErr /= seqLength // count error per symbol
                println(" | Pe = $pErr")
                if (r < entropy) break // if speed < entropy - P[error] grows
                n--
            }
        }
    }
}

/**
 * log2 by base permutation formula from log10
 */
fun log2(a: Double): Double = Math.log10(a) / Math.log10(2.0)